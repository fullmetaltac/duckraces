package ua.com.satan.duckraces.canvas

import android.graphics.Canvas
import android.graphics.Color
import ua.com.satan.duckraces.entities.Circle
import ua.com.satan.duckraces.entities.Line

class DrawThread(var duckThreads: List<DuckThread>, raceView: RaceView, val callback: () -> Unit, var start: Boolean = true) : Thread() {
    var const = raceView.const
    var holder = raceView.holder
    var leaders = raceView.leaders
    var obstacles = raceView.obstacles

    override fun run() {
        while (start) {
            val canvas = holder.lockCanvas()
            draw(canvas)
            holder.unlockCanvasAndPost(canvas)
            checkFinish()
        }
    }

    fun checkFinish() {
        if (!duckThreads.map { it -> it.start }.contains(true)) {
            start = false
            callback()
        }
    }

    fun draw(canvas: Canvas) {
        canvas.drawColor(Color.WHITE)
        duckThreads.forEach {
            Line(it.num, const).draw(canvas)
            Circle(it.point).draw(canvas, false)
        }
        obstacles.forEach { Circle(it).draw(canvas) }
    }
}