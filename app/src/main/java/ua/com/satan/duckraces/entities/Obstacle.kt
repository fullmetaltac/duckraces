package ua.com.satan.duckraces.entities

import android.content.Context
import android.content.Context.AUDIO_SERVICE
import android.graphics.PointF
import android.media.AudioManager
import ua.com.satan.duckraces.ScreenUtils
import java.util.concurrent.CopyOnWriteArrayList


class Obstacle(point: PointF) {

    companion object {
        private const val obstacleCount = 20

        private fun getVolume(context: Context): Int {
            val audioManager = context.getSystemService(AUDIO_SERVICE) as AudioManager
            return audioManager.getStreamVolume(AudioManager.STREAM_SYSTEM)
        }

        fun generateObstacles(utils: ScreenUtils): CopyOnWriteArrayList<PointF> {
            var obstacles = CopyOnWriteArrayList<PointF>()

            for (i in 1 until obstacleCount) {
                val upperBound = (i * utils.height / obstacleCount).toInt()
                val lowerBound = if (i == 0) 0 else ((i - 1) * utils.height / obstacleCount).toInt()
                val x = utils.colx(ScreenUtils.rand(0, 5).toInt()) + utils.delta
                val y = ScreenUtils.rand(lowerBound, upperBound)
                obstacles.add(PointF(x, y))
            }

            obstacles = CopyOnWriteArrayList(obstacles.filter { it -> it.x != (utils.colx(getVolume(utils.context)) + utils.delta) })
            return obstacles
        }
    }
}