package ua.com.satan.duckraces.canvas

import android.content.Context
import android.content.Intent
import android.graphics.PointF
import android.view.SurfaceHolder
import android.view.SurfaceView
import ua.com.satan.duckraces.MainActivity
import ua.com.satan.duckraces.R
import ua.com.satan.duckraces.ScreenUtils
import ua.com.satan.duckraces.broadcast.RandIntentService
import ua.com.satan.duckraces.entities.Obstacle
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class RaceView(context: Context) : SurfaceView(context), SurfaceHolder.Callback {

    val const = ScreenUtils(context)
    var executorService: ExecutorService
    var leaders: CopyOnWriteArrayList<Int> = CopyOnWriteArrayList()
    var obstacles: CopyOnWriteArrayList<PointF> = Obstacle.generateObstacles(const)

    init {
        holder.addCallback(this)
        executorService = Executors.newFixedThreadPool(threadCount + 1)
    }

    companion object {
        private const val threadCount = 5
    }

    override fun surfaceCreated(holder: SurfaceHolder) {
        val duckThreads = (0 until threadCount).map { it -> DuckThread(it, this) }
        duckThreads.forEach { it -> executorService.execute(it) }
        executorService.execute(DrawThread(duckThreads, this, ::startIntentService))
    }

    fun startIntentService() {
        val randService = Intent(context, RandIntentService::class.java)
        (context as MainActivity).showProgressDialog()
        randService.putExtra(context.getString(R.string.leaders), leaders.toIntArray())
        context.startService(randService)
    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {}
    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {}
}
