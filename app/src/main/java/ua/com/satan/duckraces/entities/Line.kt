package ua.com.satan.duckraces.entities

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import ua.com.satan.duckraces.ScreenUtils

class Line(private var num: Int, var utils: ScreenUtils) {
    val bluePaint = Paint()

    init {
        bluePaint.color = Color.BLUE
    }

    fun draw(canvas: Canvas) {
        val x = utils.colx(num + 1)
        canvas.drawLine(x, 0F, x, utils.height, bluePaint)
    }
}