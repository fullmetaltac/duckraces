package ua.com.satan.duckraces

import android.app.ProgressDialog
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import ua.com.satan.duckraces.broadcast.RandReciever
import ua.com.satan.duckraces.canvas.RaceView

class MainActivity : AppCompatActivity() {
    lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupProgressDialog()
        setIntentFilter()
        setContentView(RaceView(this))
        supportActionBar?.hide()
    }

    fun showProgressDialog() {
        runOnUiThread { progressDialog.show() }
    }

    fun setupProgressDialog() {
        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Judges making decision")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)
    }

    fun setIntentFilter() {
        val filter = IntentFilter(RandReciever.action)
        filter.addCategory(Intent.CATEGORY_DEFAULT)
        registerReceiver(RandReciever(progressDialog), filter)
    }
}
