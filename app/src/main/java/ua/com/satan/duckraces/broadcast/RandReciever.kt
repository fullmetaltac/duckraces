package ua.com.satan.duckraces.broadcast

import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast


class RandReciever(var progressDialog: ProgressDialog?) : BroadcastReceiver() {
    companion object {
        val action = "generateRandoms"
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        val msg = intent?.getIntArrayExtra("leaders")
        progressDialog?.dismiss()
        Toast.makeText(context, "Leaderboard is: ${msg?.joinToString("/")}", Toast.LENGTH_LONG).show();
    }
}