package ua.com.satan.duckraces.entities

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.PointF

class Circle(private var point: PointF) {
    private val redPaint = Paint()
    private val bluePaint = Paint()

    init {
        redPaint.color = Color.RED
        bluePaint.color = Color.BLUE
    }

    companion object {
        const val radius = 30F
    }

    fun draw(canvas: Canvas, isObstacle: Boolean = true) {
        canvas.drawCircle(point.x, point.y, radius, if (isObstacle) redPaint else bluePaint)
    }
}