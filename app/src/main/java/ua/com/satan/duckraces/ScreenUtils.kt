package ua.com.satan.duckraces

import android.content.Context
import java.util.*

class ScreenUtils(var context: Context) {

    val metrics = context.resources.displayMetrics
    val width = metrics.widthPixels.toFloat()
    val height = metrics.heightPixels.toFloat()

    val tracksCount = 5
    val delta = width / (tracksCount * 2)

    fun colx(num: Int): Float {
        return num * width / tracksCount
    }

    companion object {
        fun rand(from: Int, to: Int): Float {
            return (Random().nextInt(to - from) + from).toFloat()
        }
    }
}