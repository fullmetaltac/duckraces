package ua.com.satan.duckraces.broadcast

import android.app.IntentService
import android.content.Intent
import ua.com.satan.duckraces.ScreenUtils


class RandIntentService : IntentService("RandIntentService") {
    val arraySize: Int = 10000

    override fun onHandleIntent(intent: Intent?) {
        Array(arraySize, { x -> ScreenUtils.rand(0, arraySize) }).sort()
        Thread.sleep(1000)

        val broadcastIntent = Intent()
        broadcastIntent.action = RandReciever.action
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT)
        broadcastIntent.putExtra("leaders", intent?.getIntArrayExtra("leaders"))
        sendBroadcast(broadcastIntent)
    }
}