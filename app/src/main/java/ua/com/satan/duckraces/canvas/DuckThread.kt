package ua.com.satan.duckraces.canvas

import android.graphics.PointF
import ua.com.satan.duckraces.entities.Circle

class DuckThread(var num: Int, raceView: RaceView, var start: Boolean = true) : Thread() {
    var const = raceView.const
    var leaders = raceView.leaders
    var obstacles = raceView.obstacles
    var point: PointF = PointF((const.colx(num) + const.delta), Circle.radius)

    companion object {
        private const val speed = 1L
        private const val timeOut = 50L
        private const val finishLine = Circle.radius * 3
    }

    override fun run() {
        while (start) {
            update(point)
            checkFinish(point)
        }
    }

    fun update(point: PointF) {
        Thread.sleep(speed)
        checkForObstacle()
        point.y++
    }

    fun checkFinish(point: PointF) {
        if (point.y >= (const.height - finishLine)) {
            this.start = false
            leaders.add(num + 1)
        }
    }

    fun checkForObstacle() {
        var index = -1

        obstacles.forEach {
            if (it.x == point.x && Math.abs(point.y - it.y) < Circle.radius) {
                index = obstacles.indexOf(it)
            }
        }

        if (index != -1) {
            obstacles.removeAt(index)
            Thread.sleep(timeOut)
        }
    }
}